CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Features
 * Configuration
 * Browser Compatibility
 * Maintainers


INTRODUCTION
------------

Creativity is a **Drupal 8** theme and is a sub theme of Bootstrap. Creativity is  under active development and is powered by **OpenSense Labs**.


REQUIREMENTS
------------

No special requirements.


INSTALLATION
------------

	*	Install as you would normally install a contributed Drupal theme.


FEATURES
--------

	* Bootstrap based theme for Business profile.
	* Responsive and fully customisable.
	* It is a generic theme and is best suited for businesses ,portfolio & magazine sites.
	* Moreover, it can be used as a base theme to give your own theme a very specific business profile look.

CONFIGURATION
-------------

No configuration is needed.


BROWSER COMPATIBILITY
---------------------

The theme has been tested on Firefox, Chrome and Safari browsers.


MAINTAINERS
-----------

	* OpenSense Labs - https://www.drupal.org/opensense-labs
