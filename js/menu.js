(function ($) {
  $(".closebtn").click(function () {
    closeNav();
  });
  $("#main span").click(function () {
    openNav();
  });
  function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft = "0";
  }
})(jQuery)
